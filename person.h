#pragma once
#include<iostream>
#include<string>
#include<vector>
//#include"Header.h"

using namespace std;

class people
{
private:
	
public:
	const string name;
	vector<int> booksOnHand;
	vector<int> returnedBooks;

	people() : name("none")
	{}

	people(const string newName) : name(newName)
	{}

	void takeBook(vector <Biblioteka>& listOfBooks, int id)
	{
		int position = findBook(listOfBooks, id);
		if (position != -1)
		{
			booksOnHand.push_back(id);
			listOfBooks[position].changeStatus();
		}
		else
			cout << "this book doesn't exist" << endl;
	}

	void returnBook(vector <Biblioteka>& listOfBooks, int id)
	{
		int position = findBook(listOfBooks, id);

		if (position != -1)
		{
			for (int i = 0; i < booksOnHand.size(); i++)
			{
				if (booksOnHand[i] == id)
				{
					swap(booksOnHand[i], booksOnHand[booksOnHand.size() - 1]);
				}
			}

			booksOnHand.pop_back();
			listOfBooks[position].changeStatus();
			returnedBooks.push_back(id);
		}
		else
			cout << "this book not from library" << endl;
	}

	void chekDebts(vector <Biblioteka>& listOfBooks)
	{
		cout << "DEBTS:" << endl;
		for (int i = 0; i < booksOnHand.size(); i++)
		{
			int position = findBook(listOfBooks, booksOnHand[i]);
			listOfBooks[position].showInformationForUser();
			cout << endl;
		}

		cout << "RETURND:" << endl;
		for (int i = 0; i < returnedBooks.size(); i++)
		{
			int position = findBook(listOfBooks, returnedBooks[i]);
			listOfBooks[position].showInformationForUser();
			cout << endl;
		}
	}
///////////////////////////////////////////////////////////////
	int findBook(vector <Biblioteka> listOfBooks, int id)
	{
		for (int i = 0; i < listOfBooks.size(); i++)
		{
			if (listOfBooks[i].getId() == id)
			{
				return i;
				break;
			}
		}
		return -1;
	}
};


