#pragma once
#include<iostream>
#include<string>
#include<vector>

using namespace std;

class Biblioteka
{
private:


public:
	int id;
	string name;
	bool ifBookAvailable;

	Biblioteka(int newId, const string& newName) : id(newId), name(newName)
	{
		ifBookAvailable = 1;
	}

	Biblioteka() : id(-1), name(""), ifBookAvailable(0)
	{}

	void showBook()
	{
		cout << "id: " << id << endl;
		cout << "name: " << name << endl;
		if (ifBookAvailable)
			cout << "book is available now" << endl;
		else
			cout << "book isn't available now" << endl;
	}

	void addBook(vector <Biblioteka> &listOfBooks, int newId, const string& newName)
	{
		//id = newId;
		//name = newName;
		//ifBookAvailable = 1;

		Biblioteka bs(newId, newName);
		listOfBooks.push_back(bs);
	};

	int getId()
	{
		return id;
	}

	void changeStatus()
	{
		ifBookAvailable = !ifBookAvailable;
	}

	void showInformationForUser()
	{
		cout << "id: " << id << endl;
		cout << "name: " << name << endl;
	}

	void write(ostream& os)
	{
		os.write((char*)&id, sizeof(id));
		size_t len = name.length() + 1;
		os.write((char*)&len, sizeof(len));
		os.write((char*)name.c_str(), len);
		os.write((char*)&ifBookAvailable, sizeof(ifBookAvailable));
	}

	void read(istream& in)
	{
		in.read((char*)&id, sizeof(id));
		size_t len;
		in.read((char*)&len, sizeof(len));
		char* buf = new char[len];
		in.read(buf, len);
		name = buf;
		delete[]buf;
		in.read((char*)&ifBookAvailable, sizeof(ifBookAvailable));
	}
};



