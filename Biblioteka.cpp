﻿#include <iostream>
#include <vector>
#include <iomanip>
#include <stdio.h>
#include <conio.h>
#include <string>
#include <windows.h>
#include "library.h"
#include "file.h"
#include "person.h"

using namespace std;

void GoToXY(short x, short y)
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD position = { x, y };
    SetConsoleCursorPosition(hStdout, position);
}

void MenuToScreen(int N, vector<string>& menu)
{
    system("cls");
    for (int i = 0; i < N; i++)
    {
        cout << menu[i] << endl;
    }
    GoToXY(22, 0);
}

int findPerson(int personId, vector <person> &listOfPersons)
{
    for (int i = 0; i < listOfPersons.size(); i++)
    {
        if(listOfPersons[i].getId() == personId)
            return listOfPersons[i].getId();
    }
}

int main()
{
    vector <library> listOfBooks;
    vector <person> listOfPersons;
    file f("data");

    int N = 10;
    vector<string> menu(N);
    menu[0] = "downloud data to file";
    menu[1] = "show data from file";
    menu[2] = "show all books";
    menu[3] = "add book";
    menu[4] = "add person";
    menu[5] = "show all persons";
    menu[6] = "give book";
    menu[7] = "return book";
    menu[8] = "show debts";
    menu[9] = "Exit";

    MenuToScreen(N, menu);
    char ch, bs;
    int unicode, NumStr = 0;
    bool IfExit = false;
    do
    {
        ch = _getch();
        unicode = static_cast<int>(ch);
        switch (unicode)
        {
            case 80:
            {
                if (NumStr < N - 1) 
                {
                    NumStr++;
                    GoToXY(22, NumStr);
                }
                break;
            }

            case 72:
            {
                if (NumStr > 0) {
                    NumStr--;
                    GoToXY(22, NumStr);
                }
                break;
            }

            case 13:
            {
                switch (NumStr)
                {
                    case 0:
                    {
                        system("cls"); 
                        f.writeBooksToFile();
                        system("pause"); 
                        break;
                    }
                    case 1: 
                    {
                        system("cls");
                        f.showBooksInFile();
                        system("pause");
                        break;
                    }
                    case 2: 
                    {
                        system("cls"); 
                        for (int i = 0; i < listOfBooks.size(); i++)
                        {
                            listOfBooks[i].showBook();
                        }
                        system("pause"); 
                        break;
                    }
                    case 3: 
                    {
                        system("cls");
                        library newBook;
                        newBook.addBook();
                        system("pause"); 
                        break;
                    }
                    case 4: 
                    {
                        system("cls");
                        person newPerson;
                        newPerson.addPerson();
                        system("pause");
                        break;
                    }
                    case 5: 
                    {
                        system("cls");
                        for (int i = 0; i < listOfPersons.size(); i++)
                        {
                            listOfPersons[i].showPerson();
                        }
                        system("pause");
                        break;
                    }
                    case 6: 
                    {
                        system("cls"); 
                        int personId, bookId;
                        cin>> personId >> bookId;
                        listOfPersons[findPerson(personId, listOfPersons)].takeBook(bookId);
                        system("pause"); 
                        break;
                    }
                    case 7: 
                    {
                        system("cls");
                        int personId, bookId;
                        cin >> personId >> bookId;
                        listOfPersons[findPerson(personId, listOfPersons)].returnBook(bookId);
                        system("pause");
                        break;
                    }
                    case 8: 
                    {
                        system("cls");
                        int personId;
                        cin >> personId;
                        listOfPersons[findPerson(personId, listOfPersons)].chekDebts();
                        system("pause");
                        break;
                    }
                    case 9: IfExit = true; break;
                }

                if (IfExit) 
                {
                    system("cls"); unicode = 27;
                }
                else
                    MenuToScreen(N, menu);
            break;
            }

            case 27:
            {
                system("cls");
                break;
            }
        }
    }
    while (unicode != 27);

    return 0;
}