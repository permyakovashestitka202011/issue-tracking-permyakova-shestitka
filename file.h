#pragma once
#include<iostream>
#include<string>
#include<vector>
#include<fstream>
#include "library.h"
#include "person.h"//
//#include "Header.h"

using namespace std;

std::ostream& operator<<(std::ostream& os, const Biblioteka& value) {
	os << value.id << " " << value.name << endl << value.ifBookAvailable << endl;
	return os;
}

std::istream& operator>>(std::istream& is, Biblioteka& value) {
	is >> value.id;
	getline(is, value.name);
	is >> value.ifBookAvailable;
	return is;
}

class file
{
public:
	fstream recordFile;
	const string s;

	file(const string ns) : s(ns)
	{}


	void writeBooks(vector <Biblioteka>& listOfBooks)
	{
		recordFile.open(s, ofstream::out);

		for (int i = 0; i < listOfBooks.size(); i++)
		{
			recordFile << listOfBooks[i];
			//recordFile << listOfBooks[1];
		}
		recordFile.close();
	}

	void showBooksFile()
	{
		recordFile.open(s, ifstream::in);
		Biblioteka bs;

		while(recordFile >> bs)
			bs.showBook();
		//recordFile >> bs;
		//bs.showBook();
		recordFile.close();
	}
}; 
